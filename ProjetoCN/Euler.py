from math import factorial
var = float(input("Digite o valor de X para e^x: "))
e = 1

for i in range(1, 90):
    e += (var**i)/factorial(i)

print("e^{} = {}".format(var, e))

