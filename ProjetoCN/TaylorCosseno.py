from math import factorial, pi

x= float(input("Informe o valor do arco em graus:"))

n=(x*pi)/180
cosseno= n
fat= 1
sign= 1
for i in range(1,80):
    sign= - sign
    cosseno += sign * (n ** (2 * i)) / factorial(2 * i)

print("O cosseno de ({}) = {}".format(x, cosseno))
