from math import pi, factorial

var= float(input("Informe o valor do arco em graus:"))

x= (var * 180) / pi
seno= x
sign= 1
for n in range(1, 50):
    seno += sign*(x ** (2*n+1)) / factorial(2*n+1)
    sign= - sign

print("O seno de ({}) = {}".format(var, seno))
