x = float(input("O valor de x para Ln(1+x): "))
ln=0
sinal=1

if x<=-1 or x>1:
    print("Valor inválido")
else:
    for i in range(1,60):
        sinal = -sinal
        ln -= sinal*(x**i)/i
    print(" ln(1+{}) = {} ".format(x,ln))
