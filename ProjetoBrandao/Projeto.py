from string import *
from cmath import *
from random import *

# DECLARAÇÃO DAS LISTAS A SEREM UTILIZADAS
cliente_nome = []
cliente_cpf = []
cliente_id = []
cliente = [cliente_nome, cliente_cpf, cliente_id]
produto_nome = []
produto_valor = []
produto_desc = []
produto_qt = []
produto = [produto_nome, produto_desc, produto_valor, produto_qt]


# FUNÇÃO DO MENU
def menu():
    print('||\tMenu\t||')
    print('||\t1 - Cadastrar Cliente\t||')
    print('||\t2 - Buscar Cliente\t||')
    print('||\t3 - Alterar Cliente\t||')
    print('||\t4 - Excluir Cliente\t||')
    print('||\t5 - Cadastrar Produto\t||')
    print('||\t6 - Buscar Produto\t||')
    print('||\t7 - Alterar Produto\t||')
    print('||\t8 - Excluir Produto\t||')
    print('||\t9 - Realizar Venda\t||')
    print('||\t10 - Consultar Vendas\t||')
    print('||\t11 - Sair\t||')
    opt = int(input('\n\tDigite a opção desejada:'))
    return opt


def cadastra_cliente():
    client_nome = str(input('Digite o nome do cliente: '))
    client_cpf = input('Digite o cpf do cliente: ')
    client_id = int(input('Digite o ID: '))
    if client_cpf in cliente_cpf:
        print('Cliente já cadastrado')
    else:
        cliente_nome.append(client_nome)
        cliente_cpf.append(client_cpf)
        cliente_id.append(client_id)
        print('Cliente Cadastrado!')
    pass


def buscar_cliente():
    client = int(input('Digite o ID do cliente a ser buscado: '))
    if client in cliente_id:
        print('Cliente encontrado: ', cliente_nome[client],cliente_cpf[client])
    else:
        print('Cliente não encontrado!')
    pass


def alterar_cliente():
    pass


def excluir_cliente():
    client = str(input('Digite o nome a ser excluido: '))
    pass


def cadastra_produto():
    prod_nome = str(input('Digite o nome do produto: '))
    prod_desc = str(input('Digite a descrição do produto: '))
    prod_valor = float(input('Digite o valor do produto: '))
    prod_qt = int(input('Digite a quantidade em estoque: '))
    if prod_nome in produto_nome:
        print('Produto já cadastrado')
    else:
        produto_nome.append(prod_nome)
        produto_desc.append(prod_desc)
        produto_valor.append(prod_valor)
        produto_qt.append(prod_qt)
        print('Produto Cadastrado!')
    pass


def buscar_produto():
    prod = str(input('Digite o produto a ser buscado: '))
    if prod in produto_nome:
        print('Produto encontrado: ', prod)
    else:
        print('Produto não encontrado!')
    pass


def alterar_produto():
    pass


def excluir_produto():
    prod = str(input('Digite o nome do produto a ser excluido'))
    if prod in produto:
        produto_nome.remove(prod)
    else:
        print('Produto não cadastrado!')
    pass


def realiza_venda():
    pass


def consulta_vendas():
    pass


opcao = menu()
while opcao != 11:
    if opcao == 1:
        cadastra_cliente()
    elif opcao == 2:
        buscar_cliente()
    elif opcao == 3:
        alterar_cliente()
    elif opcao == 4:
        excluir_cliente()
    elif opcao == 5:
        cadastra_produto()
    elif opcao == 6:
        buscar_produto()
    elif opcao == 7:
        alterar_produto()
    elif opcao == 8:
        excluir_produto()
    elif opcao == 9:
        realiza_venda()
    elif opcao == 10:
        consulta_vendas()

    opcao = menu()
