def menu():
    print("-------------------------------")
    print("||   1 - Ler Arquivo         ||")
    print("||   2 - Imprimir Arquivo    ||")
    print("||   3 - Sair                ||")
    print("-------------------------------")
    opcao = int(input("\nDigite a opção desejada: "))
    return opcao

def leArquivo():
    nomeArquivo = input("Digite o nome do arquivo a ser lido: ")
    arquivo = open(nomeArquivo)
    return arquivo

def imprimeArquivo(arquivo):
    texto = arquivo.readlines()
    return texto


op=0
while op != 3:
    op = menu()
    if op is 1:
        arq = leArquivo();
    elif op is 2:
        texto = imprimeArquivo(arq)
        print(texto)
