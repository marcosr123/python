import math
s1 = [1]
s2 = [-0.5, 1.5]
s3 = [5./12, -4./3, 23./12]
s4 = [-3./8, 37/24, -59./24, 55./24]
s5 = [251./720, -637./360, 109./30, -1387./360, 1901./720]
def adams_bashforth(f, ta, tb, xa, n, sn=s5):
    last_n = []
    h = (tb - ta) / float(n)
    t = ta
    x = xa
    for i in range(len(sn)):
        last_n.append(h * f(t, x))
        x += last_n[-1]
        t += h
    for i in range(n):
        x += h * sum([last_n[i] * sn[i] for i in range(len(sn))])
        last_n = last_n[1:]
        last_n.append(f(t, x))
        t += h
    return x

def f(t, x):
    return t * math.sqrt(x)
print(adams_bashforth(f, 0, 1, 10, 1000, s5))
