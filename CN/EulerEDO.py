import math as m
def Euler(f, xa, xb, ya, n):
      h = (xb - xa) / float(n)
      x = xa
      y = ya
      for i in range(n):
          y += h * f(x, y)
          x += h
      return y

def Euler2(f, xa, xb, ya, y1a, n):
      h = (xb - xa) / float(n)
      x = xa
      y = ya
      y1 = y1a
      for i in range(n):
          y1 += h * f(x, y, y1)
          y += h * y1
          x += h
      return y


print(Euler(lambda x, y: m.cos(x) + m.sin(y), 0, 1, 1, 1000))
print(Euler2(lambda x, y, y1: m.sin(x * y) - y1, 0, 1, 1, 1, 1000))
