cat = 12
hip = 1

'''Marcos Richard Barros Cutrim'''

def f(x):
    return x**2 - 2*(cat**2)

def hipotenusa(hip, hip1):
    a = (hip + hip1) / 2.0
    for i in range (20):
        if f(a) == 0:
            return a
        elif f(hip)*f(a) < 0:
            hip1 = a
        else :
            hip = a
        a = (hip + hip1) / 2.0
    return a


hipot = hipotenusa(1, 50)
print("Valor aproximado da hipotenusa é :",hipot)
print("Substituindo na função, temos: ",f(hipot))




